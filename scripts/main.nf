#!/usr/bin/env nextflow

nextflow.enable.dsl=2
params.outputFolder = "" // "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/users/kdasilva/20240125_seq4epi_primers/nf_results_2024-01-01_2024-01-29"
params.reference_fasta = "" // "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/users/kdasilva/references/sars-cov-2/reference.fasta"
params.reference_gff = "" // "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/autorun/seqworkflows/virflow_dev/assets/sars-cov-2/reference.gff"
params.primers_fasta = "" // "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/users/kdasilva/primers/sarscov2/primers_sarscov2_qpcr.fasta"
params.metadata = "" // tar or tsv "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/projects/GISAID/metadata_tsv_2024_02_26.tar.xz"
params.start_date = "" // "2024-01-01"
params.end_date = "" // "2024-01-29"
params.gisaid_sequences = "" // tar or fasta "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/projects/GISAID/sequences_fasta_2024_02_26.tar.xz"
params.virus = "SARS"
params.regions = "/pasteur/zeus/projets/p01/CNRVIR_bioinfoSEQ/users/kdasilva/cnrvir_git/priomon/scripts/assets/rsv_region_file.tsv"

process GenerateRSVMetadata {

    label 'python'

    publishDir "${params.outputFolder}/02_gisaid_data", mode : 'copy'

    input:
    path sequences

    output:
    path "metadata.tsv"

    script:
    """
    rsv_fasta2tsv.py -f ${sequences} -r ${params.regions} -o metadata.tsv
    """
}

// Task to map primers multifasta to reference fasta using BWA
process MapPrimers {

    tag "${primers_fasta.baseName}"

    label 'bwa'

    input:
    path reference_fasta
    path primers_fasta

    output:
    path "${primers_fasta.baseName}.mapped.sam"

    script:
    """
    bwa index ${reference_fasta} || { echo "Error indexing reference."; exit 1; }
    bwa mem ${reference_fasta} ${primers_fasta} -k 8 -T 10 > ${primers_fasta.baseName}.mapped.sam || { echo "Error in BWA mem."; exit 1; }
    """
}

process SamToBam {

    tag "${sam_file.baseName}"

    label 'samtools'

    publishDir "${params.outputFolder}/01_primers_mapping", mode : 'copy'

    input:
    path sam_file

    output:
    path "${sam_file.baseName}.sorted.bam"
    path "${sam_file.baseName}.unmapped.sam"

    script:
    """
    samtools view -bS ${sam_file} > "${sam_file.baseName}.bam" || { echo "Error converting SAM to BAM."; exit 1; }
    samtools sort -o "${sam_file.baseName}.sorted.bam" "${sam_file.baseName}.bam" || { echo "Error sorting BAM."; exit 1; }
    samtools view -f 4 "${sam_file.baseName}.sorted.bam" > "${sam_file.baseName}.unmapped.sam" || { echo "Error extracting unmapped primers."; exit 1; }
    """
}

process BamToBed {

    tag "${bam_file.baseName}"

    label 'bedtools'

    publishDir "${params.outputFolder}/01_primers_mapping", mode : 'copy'

    input:
    path bam_file

    output:
    path "${bam_file.baseName}.bed"

    script:
    """
    bedtools bamtobed -i "${bam_file}" > "${bam_file.baseName}.bed" || { echo "Error converting BAM to BED."; exit 1; }
    """
}

process AddSeqToBed {

    tag "${primers_fasta.baseName}"

    label 'python'

    publishDir "${params.outputFolder}/01_primers_mapping", mode : 'copy'

    input:
    path primers_fasta
    path primers_bed

    output:
    path "${primers_fasta.baseName}.primer.bed"

    script:
    """
    add_seq_to_bed.py -f ${primers_fasta} -b ${primers_bed} -o ${primers_fasta.baseName}.primer.bed
    """
}

process ExtractMetadata {

    tag "${metadata.baseName}"

    publishDir "${params.outputFolder}/02_gisaid_data", mode : 'copy'

    input:
    path metadata
    val start_date
    val end_date

    output:
    path "metadata_filtered.tsv"

    script:
    def args = [
        "-m ${metadata}",
        "-o metadata_filtered.tsv",
        "-v ${params.virus}"
    ]

    if (start_date) args.add("-s ${start_date}")
    if (end_date) args.add("-e ${end_date}")

    """
    filter_metadata_by_date.sh ${args.join(' ')}
    """
}

process ExtractGisaidSequences {

    label 'pythonmore'

    tag "${sequences_tar.baseName}"

    publishDir "${params.outputFolder}/02_gisaid_data", mode : 'copy'

    input:
    path sequences_tar
    path metadata_tsv

    output:
    path "sequences_filtered.fasta"

    script:
    """
    filter_gisaid_sequences.py -f ${sequences_tar} -m ${metadata_tsv} -t 4 -o "sequences_filtered.fasta" -v ${params.virus}
    """
}

process Nextalign {

    label 'nextclade3'

    tag "${sequences_fasta.baseName}"

    publishDir "${params.outputFolder}/03_nextclade3_outputs", mode : 'copy'

    input:
    path ref_fasta
    path ref_gff
    path sequences_fasta

    output:
    path "${sequences_fasta.baseName}.aligned.fasta"
    path "${sequences_fasta.baseName}.nextclade.tsv"

    script:
    """
    nextclade run --input-ref=${ref_fasta} --input-annotation=${ref_gff} --output-fasta=${sequences_fasta.baseName}.aligned.fasta --output-tsv=${sequences_fasta.baseName}.nextclade.tsv ${sequences_fasta} 
    """
}

process ExtractMutationsPrimers {

    label 'python'

    publishDir "${params.outputFolder}/04_mutations", mode : 'copy'

    input:
    path aligned_fasta
    path primers_bed
    path metadata_tsv

    output:
    path "*"

    script:
    """
    get_mutations_primers.py -f ${aligned_fasta} -b ${primers_bed} -d ${metadata_tsv} -o .
    """
}

// Workflow
workflow {

    ref = file(params.reference_fasta)
    primers = file(params.primers_fasta)
    sequences = file(params.gisaid_sequences)
    gff = file(params.reference_gff)

    if (params.virus == "RSV") {
        metadata = GenerateRSVMetadata(sequences)
    } else {
        metadata = file(params.metadata)
    }

    // Primers mapping
    sam_file = MapPrimers(ref,primers)
    samtools_outputs = SamToBam(sam_file)
    bed_file = BamToBed(samtools_outputs[0])
    primers_bed = AddSeqToBed(primers,bed_file)

    // Extract metadata
    metadata_filt = ExtractMetadata(metadata, params.start_date, params.end_date)

    // Extract gisaid sequences
    sequences_filt = ExtractGisaidSequences(sequences,metadata_filt)

    // Align query sequences to reference
    nextalign_outputs = Nextalign(ref,gff,sequences_filt)
    aligned_fasta = nextalign_outputs[0]

    // Extract mutations
    ExtractMutationsPrimers(aligned_fasta, primers_bed, metadata_filt)
}
