#!/usr/bin/env python

import argparse

# Function to read sequences from multifasta file
def read_fasta(fasta_file):
    sequences = {}
    current_key = None
    current_seq = ''
    with open(fasta_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith('>'):  # Header line
                if current_key is not None:
                    sequences[current_key] = current_seq
                    current_seq = ''  # Reset sequence
                current_key = line[1:]  # Exclude '>'
            else:  # Sequence line
                current_seq += line
        # Add the last sequence
        if current_key is not None:
            sequences[current_key] = current_seq
    return sequences

# Function to add sequence column to bed file
def add_sequence_column(bed_file, fasta_file, output_file):
    sequences = read_fasta(fasta_file)
    with open(bed_file, "r") as f_in, open(output_file, "w") as f_out:
        for line in f_in:
            fields = line.strip().split("\t")
            seq_id = fields[3]
            if seq_id in sequences:
                sequence = sequences[seq_id]
            else:
                sequence = ""
            fields.append(sequence)
            f_out.write("\t".join(fields) + "\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="add sequence to bed file")
    parser.add_argument("-f","--fasta_file", required=True, help="Path to the fasta file.")
    parser.add_argument("-b","--bed_file", required=True, help="Path to the bed file.")
    parser.add_argument("-o","--output_file", required=True, help="Path to the output file.")
    args = parser.parse_args()

    add_sequence_column(args.bed_file,args.fasta_file,args.output_file)