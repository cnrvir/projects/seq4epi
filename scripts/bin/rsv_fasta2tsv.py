#!/usr/bin/env python

import argparse
import sys

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert FASTA headers to TSV format.")
    parser.add_argument("-f","--input_fasta", help="Path to the input FASTA file")
    parser.add_argument("-r","--regions_file", help="Path to the regions file")
    parser.add_argument("-o","--output_tsv", help="Path to the output TSV file")
    args = parser.parse_args()

    regions_dict = {}
    with open(args.regions_file, "r") as regions_file:
        for line in regions_file:
            columns = line.strip().split("\t")
            if len(columns) == 2:  # Ensure there are at least two columns
                key, value = columns[1], columns[0]
                regions_dict[key] = value

    try:
        with open(args.input_fasta, 'r') as fasta_file, open(args.output_tsv, 'w') as tsv_file:
            tsv_file.write("Isolate_Name\taccession_id\tcollection_date\tLocation\tClade\n")

            for line in fasta_file:
                if line.startswith(">"):
                    fields = line[1:].strip().split("|")
                    if len(fields) == 3:
                        country = fields[0].split("/")[2]
                        region = regions_dict.get(country)
                        fields.append(f"{region}/{country}")
                        fields.append("") # empty clade
                        tsv_file.write("\t".join(fields) + "\n")
                    else:
                        print(f"Warning: Header does not contain exactly 3 fields: {line.strip()}")

    except FileNotFoundError:
        print(f"Error: File not found - {args.input_fasta}")
        sys.exit(1)

    except Exception as e:
        print(f"An error occurred: {e}")
        sys.exit(1)
