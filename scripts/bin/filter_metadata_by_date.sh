#!/bin/bash

#######################
# 1. Manage arguments #
#######################

# Help message function
show_help() {
    echo "Usage: $0 -m <metadata> [-s <start_date>] [-e <end_date>] -o <output>"
    echo ""
    echo "Options:"
    echo "  -m, --metadata       Path to the metadata file (mandatory)"
    echo "  -o, --output        Path to output file (mandatory)"
    echo "  -s, --start_date      Start date for filtering (optional)"
    echo "  -e, --end_date        End date for filtering (optional)"
    echo "  -v, --virus       SARS or FLU or RSV (mandatory)"
    echo "  -h, --help        Display this help message"
}

# Use getopt to parse the command-line arguments
OPTIONS=$(getopt -o m:s:e:o:v:h --long metadata:,start_date:,end_date:,output:,virus:,help -- "$@")
if [ $? -ne 0 ]; then
    show_help
    exit 1
fi

# Evaluate the parsed options
eval set -- "$OPTIONS"

# Initialize variables
metadata_file=""
start_date=""
end_date=""
output_file=""
virus=""

# Parse options
while true; do
    case "$1" in
        -m | --metadata)
            metadata_file="$2"
            shift 2
            ;;
        -s | --start_date)
            start_date="$2"
            shift 2
            ;;
        -e | --end_date)
            end_date="$2"
            shift 2
            ;;
        -o | --output)
            output_file="$2"
            shift 2
            ;;
        -v | --virus)
            virus="$2"
            shift 2
            ;;
        -h | --help)
            show_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            show_help
            exit 1
            ;;
    esac
done

# Check for mandatory arguments
if [ -z "$metadata_file" ] || [ -z "$output_file" ]; then
    echo "Error: Both input file and output file are mandatory."
    show_help
    exit 1
fi

# Extract TSV file from tar.xz, stream it through awk to filter by date range, and save the output

if [[ "$virus" == "FLU" ]]; then
    date_column=26
elif [[ "$virus" == "RSV" ]]; then
    date_column=2
else
    date_column=6
fi

awk_command='BEGIN {FS="\t"; OFS="\t"}'
if [[ -n "$start_date" && -n "$end_date" ]]; then
    awk_command+=" (\$$date_column >= start && \$$date_column <= end)"
elif [[ -n "$start_date" ]]; then
    awk_command+=" (\$$date_column >= start)"
elif [[ -n "$end_date" ]]; then
    awk_command+=" (\$$date_column <= end)"
else
    awk_command+=' {print}'
fi

if [[ "$metadata_file" == *.tar* ]]; then
    tar -xJOf "$metadata_file" "*.tsv" | {
        head -n 1 # Print the header line
        awk -v start="$start_date" -v end="$end_date" "$awk_command"
    } > "$output_file"
else
    {
        head -n 1 "$metadata_file" # Print the header line
        tail -n +2 "$metadata_file" | awk -v start="$start_date" -v end="$end_date" "$awk_command"
    } > "$output_file"
fi