#!/usr/bin/env python3

import subprocess
import argparse
import csv
import io
from Bio import SeqIO

def selectByVirusName(fasta_file, metadata_file, output_file, cpus, virus):

    if virus == "FLU":
        metadata_column = 11 # Isolate_Name
        fasta_column = 0 # Isolate_Name
    elif virus == "RSV":
        metadata_column = 0 # virus_name
        fasta_column = 0 # virus_name
    else:
        metadata_column = 11
        fasta_column = 0

    # Get headers
    with open(metadata_file, newline='') as tsvfile:
        tsv_file = csv.reader(tsvfile, delimiter="\t")
        next(tsv_file,None)
        gisaidlist = [line[metadata_column].strip().replace(" ","_") for line in tsv_file]

    with open(output_file, 'wt') as outf:

        if fasta_file.endswith(".tar.xz"):
            # Execute the shell command
            proc = subprocess.Popen(["sh", "-c", f"tar -x -O -I 'xz -T{cpus}' -f {fasta_file} sequences.fasta"], stdout=subprocess.PIPE)

            # Read the output from the subprocess line by line
            for record in SeqIO.parse(io.TextIOWrapper(proc.stdout), "fasta"):
                id=record.description.split("|")[fasta_column].strip()
                if id in gisaidlist:
                    outf.write(f'>{id}\n')
                    outf.write(f'{record.seq}\n')
        elif fasta_file.endswith(".fasta"):
            for record in SeqIO.parse(fasta_file, "fasta"):
                id=record.description.split("|")[fasta_column].strip()
                if id in gisaidlist:
                    outf.write(f'>{id}\n')
                    outf.write(f'{record.seq}\n')
        else:
            raise ValueError("Error: Fasta file should be in .fasta or .tar.xz!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="select sequences by virus name")
    parser.add_argument("-f","--fasta_tar", required=True, help="Path to the fasta tar.xz.")
    parser.add_argument("-m","--metadata_tsv", required=True, help="Path to the metadata tsv.")
    parser.add_argument("-t","--cpus", required=True, help="Number of cpus")
    parser.add_argument("-o","--output_file", required=True, help="Path to the output file.")
    parser.add_argument("-v","--virus", choices=['SARS', 'FLU', 'RSV'], help="Type of virus")
    args = parser.parse_args()

    selectByVirusName(args.fasta_tar,args.metadata_tsv,args.output_file, args.cpus, args.virus)
