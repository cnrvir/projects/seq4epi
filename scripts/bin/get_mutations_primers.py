#!/usr/bin/env python3

import os
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import pandas as pd

def is_ambiguity_match(base1, base2):
    """
    Check if two bases match, considering IUPAC ambiguity codes.
    """
    iupac_codes = {
        'A': {'A'}, 'C': {'C'}, 'G': {'G'}, 'T': {'T'}, 
        'R': {'A', 'G'}, 'Y': {'C', 'T'}, 'S': {'G', 'C'}, 'W': {'A', 'T'},
        'K': {'G', 'T'}, 'M': {'A', 'C'}, 'B': {'C', 'G', 'T'},
        'D': {'A', 'G', 'T'}, 'H': {'A', 'C', 'T'}, 'V': {'A', 'C', 'G'}, 
        'N': {'A', 'C', 'G', 'T'}
    }
    return base1 in iupac_codes and base2 in iupac_codes and iupac_codes[base1] & iupac_codes[base2]


def find_mutations(primer_infos, query_seq):
    """
    Find mutations between two sequences.
    """
    primer_seq = primer_infos['seq']
    start = primer_infos['start']
    mutations = []

    for i, primer_base in enumerate(primer_seq):
        primer_base = primer_base.upper()
        query_base = query_seq[start + i].upper()

        # Ignore 'N' in either sequence
        if primer_base == 'N' or query_base == 'N':
            continue

        # Check for exact match or ambiguity match
        if primer_base == query_base or is_ambiguity_match(primer_base, query_base):
            continue

        # Handle deletions
        if query_base == '-':
            mutations.append(f"{primer_base}{start + i}-")
        else:
            # Handle substitutions
            mutations.append(f"{primer_base}{start + i}{query_base}")

    return mutations

def compute_type_penalty(mutation_list):
    complement = {'A':'T', 'C':'G', 'G':'C', 'T':'A', 'Y':'R', 'R':'Y', 'S':'S', 'W':'W', 'K':'M', 'M':'K', 'B':'V', 'V':'B', 'D':'H', 'H':'D'}
    penalty_map = {"A-A": 3, "A-G": 3, "G-A": 3, "G-G": 3,
                   "T-T": 2, "C-T": 2, "T-C": 2, "C-C": 2,
                   "A-C": 1, "C-A": 1, "G-T": 1, "T-G": 1}
    penalty_score = 0
    for mut in mutation_list:
        if "-" in mut: # deletion
            penalty_score += 1
        else:
            mm = f"{complement[mut[0]]}-{mut[-1]}"
            penalty_score += penalty_map.get(mm, 1)
    return penalty_score
        
def compute_position_penalty_primer(mutation_list,primer_infos):
    start = primer_infos['start']
    end = primer_infos['end']-1 # end is 1-based as opposed to start
    direction = primer_infos['direction']
    penalty_score = 0
    for mut in mutation_list:
        pos_mut = int(mut[1:-1])
        distance_to_start = abs(pos_mut - start)
        distance_to_end = abs(pos_mut - end)
        if direction == 'F':
            if distance_to_end == 0:
                penalty_score += 8
            elif distance_to_end == 1:
                penalty_score += 5
            elif distance_to_end == 2:
                penalty_score += 3
            elif distance_to_end == 3:
                penalty_score += 1
        elif direction == 'R':
            if distance_to_start == 0:
                penalty_score += 8
            elif distance_to_start == 1:
                penalty_score += 5
            elif distance_to_start == 2:
                penalty_score += 3
            elif distance_to_start == 3:
                penalty_score += 1
        else:
            raise ValueError("Invalid direction, must be 'F' or 'R'.")
    return penalty_score

def compute_position_penalty_probe(mutation_list,primer_infos):
    start = primer_infos['start']
    middle = start + int(len(primer_infos['seq'])/2)
    if len(primer_infos['seq']) % 2 == 0:
        middle += 0.5
    penalty_score = 0
    for mut in mutation_list:
        pos_mut = int(mut[1:-1])        
        distance_to_center = abs(pos_mut - middle)
        if distance_to_center <= 0.5:
            penalty_score += 8
        elif distance_to_center <= 1.5:
            penalty_score += 4
        elif 2 <= distance_to_center <= 4.5:
            penalty_score += 2
    return penalty_score

def compute_gc_penalty(mutation_list):
    penalty_score = sum(1 for mut in mutation_list if mut.startswith("G") or mut.startswith("C"))
    return penalty_score

def parse_fasta(file_path):
    """
    Parse a fasta file and return a dictionary of sequence names and sequences.
    """
    sequences = {}
    for record in SeqIO.parse(file_path, "fasta"):
        sequences[record.description] = str(record.seq)
    return sequences

def parse_bed(file_path):
    """
    Parse a bed file and return a dictionary of name and coordinates.
    """
    primers = {}
    with open(file_path,"r") as bed_file:
        for line in bed_file:
            line = line.split("\t")
            primer_name = line[3]
            primers[primer_name] = {}
            primers[primer_name]['start'] = int(line[1])
            primers[primer_name]['end'] = int(line[2])
            if line[5] == "-":     
                primers[primer_name]['seq'] = str(Seq(line[6].strip()).reverse_complement())
                primers[primer_name]['direction'] = "R"
            elif line[5] == "+":
                primers[primer_name]['seq'] = line[6].strip()
                primers[primer_name]['direction'] = "F"
            else:
                primers[primer_name]['seq'] = ""
                primers[primer_name]['direction'] = ""
            # if primers are at extremities and exceed the reference
            if (primers[primer_name]['end'] - primers[primer_name]['start']) < len(primers[primer_name]['seq']):
                if primers[primer_name]['direction'] == "F":
                    primers[primer_name]['seq'] = primers[primer_name]['seq'][-primers[primer_name]['end']:]
                if primers[primer_name]['direction'] == "R":
                    diff = primers[primer_name]['end'] - primers[primer_name]['start']-1
                    primers[primer_name]['seq'] = primers[primer_name]['seq'][-diff:]

    return primers

def parse_tsv(tsv_path):

    df = pd.read_csv(tsv_path, sep='\t')
    metadata = {}
    for index, row in df.iterrows():
        virus_name = row['Isolate_Name'].strip().replace(" ","_")
        if not isinstance(row['Clade'], str):
            lineage = ""
        else:
            lineage = row['Clade'].split(' ')[0]
        country = row['Location'].split("/")[1].strip()
        metadata[virus_name] = {'lineage': lineage, 'country': country}
    
    return metadata

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compare query sequences to reference sequences and find mutations.")
    parser.add_argument("-f","--aligned_fasta", required=True, help="Path to the aligned queries fasta file.")
    parser.add_argument("-b","--primers_bed", required=True, help="Path to the primers bed file.")
    parser.add_argument("-d","--metadata_tsv", required=True, help="Path to the metadata file.")
    parser.add_argument("-o","--output", required=True, help="Path to output folder.")
    args = parser.parse_args()

    # Create output folder if does not exist
    if not os.path.exists(args.output):
        os.makedirs(args.output)

    # Parse inputs
    d_sequences = parse_fasta(args.aligned_fasta) # key = seq_ID, value = sequence
    d_primers = parse_bed(args.primers_bed) # key = primer name, value = dictionary with start, end, and sequence
    d_metadata = parse_tsv(args.metadata_tsv) # key = accession_ID, value = lineage and country

    d = {}
    for query_header, query_seq in d_sequences.items():

        query_name = query_header

        for primer_name, primer_infos in d_primers.items():

            if primer_name not in d:
                d[primer_name] = {}
            if query_name not in d[primer_name]:
                d[primer_name][query_name] = {}
            
            # Find mutations between query and primer
            mutations = find_mutations(primer_infos, query_seq)

            d[primer_name][query_name]['list_mut'] = mutations
            d[primer_name][query_name]['type_penalty'] = compute_type_penalty(mutations)
            if "probe" in primer_name.lower():
                d[primer_name][query_name]['position_penalty'] = compute_position_penalty_probe(mutations, primer_infos)
            else:
                d[primer_name][query_name]['position_penalty'] = compute_position_penalty_primer(mutations, primer_infos)
            d[primer_name][query_name]['gc_penalty'] = compute_gc_penalty(mutations)
            d[primer_name][query_name]['cumul_penalty'] = 5 if len(mutations)>=4 else 0 # +5 if 4+ mutations
            d[primer_name][query_name]['total_penalty'] = d[primer_name][query_name]['type_penalty'] + d[primer_name][query_name]['position_penalty'] + d[primer_name][query_name]['gc_penalty'] + d[primer_name][query_name]['cumul_penalty']
        
    for primer_name in d.keys():

        list_all_possible_mutations = set()
        for query_name, d_query in d[primer_name].items():
            list_all_possible_mutations.update(d_query['list_mut'])
        
        # output
        with open(f"{args.output}/{primer_name}_mutations.csv", "w") as outfile: 
            # header
            header= "virus_name,lineage,country,"
            for mut in list_all_possible_mutations:
                header += f"{mut},"
            header = header + "type_penalty,position_penalty,gc_penalty,cumul_penalty,total_penalty\n"
            outfile.write(header)
            # each sequence
            for query_name, d_query in d[primer_name].items():
                line = f"{query_name},{d_metadata[query_name]['lineage']},\"{d_metadata[query_name]['country']}\"," # country can have commas!!!
                for mut in list_all_possible_mutations:
                    value = 1 if mut in d_query['list_mut'] else 0
                    line += f"{value},"
                line += f"{d_query['type_penalty']},{d_query['position_penalty']},{d_query['gc_penalty']},{d_query['cumul_penalty']},{d_query['total_penalty']}\n"
                outfile.write(line)

    