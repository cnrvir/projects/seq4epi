**SARSProbeMonitor** is a nextflow pipeline to assess the efficacy of qPCR primers and probes for detection of SARS-CoV-2. SARSProbeMonitor outputs penalty score matrices for each primer/probe and each queried sequences based on the mismatches observed between them.

Those matrices can be explored to generate heatmaps of mean average penalty at the lineage-level or the proportion of sequences for each lineage that exceed a penalty score threshold.

# Pipeline

1. Mapping primers and probes against the reference genome, and add their sequences in the generated bed file.
2. Filtering GISAID metadata to get only the metadata in the given collection date interval.
3. Filtering GISAID sequences to get only the sequences in the given collection date interval.
4. Using Nextclade, aligning filtered GISAID sequences to the reference genome.
5. Comparing the primers and probes sequences to the corresponding sub-sequences in the queries to extract the mismatches and apply penalty scores.

# Requirements

## Inputs
- GISAID sequences (tar.xz or fasta)
- GISAID metadata (tar.xz or tsv or None)
- Reference genome (fasta and gff)
- Primers/Probes sequences (fasta)

## Tools
- Nextflow
- Singularity
- Python3: singularity pulled
- bwa (0.7.17): singularity pulled
- samtools (1.16): singularity pulled
- bedtools (2.29.2): singularity pulled
- Nextclade (3.3.1): singularity pulled
- Biopython module: singularity pulled

# Usage

```
nextflow run main.nf \
--outputFolder <output directory> \
--reference_fasta <reference genome fasta for primers mapping and query sequences alignment> \
--reference_gff <reference genome gff for query sequences alignment with nextclade> \
--primers_fasta <primers and probes sequences in fasta format> \
--metadata <metadata from GISAID in tar.xz or tsv format> \
--start_date <starting collection date for filtering> \
--end_date <ending collection date for filtering> \
--gisaid_sequences <sequences from GISAID in tar.xz or fasta format>
--virus <FLU, SARS, RSV>

Rscript mutation_tables_analysis.R <working_folder>
```

# Primers

- SARS-CoV-2
	- Detection primers and probes are from patent EP3990667B1 and [nCov-IP4 update](https://www.sfm-microbiologie.org/wp-content/uploads/2021/04/Modif_IP4_probe_Update_CNRVIR210406_VE.pdf)
	- Sequencing primers are [ARTIC v5.3.2](https://github.com/artic-network/primer-schemes/tree/master/nCoV-2019/V5.3.2)
- Influenza
	- Detection primers and probes are from [WHO protocols](https://cdn.who.int/media/docs/default-source/influenza/molecular-detention-of-influenza-viruses/protocols_influenza_virus_detection_feb_2021.pdf)
	- Sequencing primers from [Hoffmann, et al. 2001](https://pubmed.ncbi.nlm.nih.gov/11811679/)
- RSV
	- Detection
	- Sequencing primers are adapted from [SimonLoriereLab](https://github.com/SimonLoriereLab/RSV_amplicons_panels/)